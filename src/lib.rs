use wasm_bindgen::prelude::*;
use qrcode::{QrCode, Version, EcLevel};
use qrcode::render::svg;

const FALLBACK_JID: &str = "kaidan@muc.kaidan.im?join";

struct Client {
    name: &'static str,
    url: &'static str,
    description: &'static str
}

fn jid_parse_name(jid: &str) -> String {
    // take only username
    let mut name: String = jid.split("@").collect::<Vec<&str>>()[0].to_string();

    // Make first character uppercase
    let new_first_char = name.chars().next().unwrap().to_uppercase().next().unwrap();
    name.remove(0);
    name.insert(0, new_first_char);
    return name;
}

fn button_text(jid: &str) -> String {
    let text: String;

    if jid.contains("?join") {
        text = format!("Join the chat room {}", jid_parse_name(&jid));
    } else {
        text = format!("Add {} to contact list", jid_parse_name(&jid));
    }

    return text;
}

fn title_text(jid: &str) -> String {
    let text: String;

    if jid.contains("?join") {
        text = format!("Invitation to {}", jid_parse_name(&jid));
    } else {
        text = format!("Invitation from {}", jid_parse_name(&jid));
    }

    return text;
}

fn heading_text(jid: &str) -> String {
    let text: String;

    if jid.contains("?join") {
        text = format!("You have been invited to {}", jid_parse_name(&jid));
    } else {
        text = format!("{} has invited you to chat", jid_parse_name(&jid));
    }

    return text;
}

fn jid_to_url(jid: &str) -> String {
    let url: String = format!("xmpp:{}", &jid);
    return url;
}


// Called when the wasm module is instantiated
#[wasm_bindgen(start)]
pub fn main() -> Result<(), JsValue> {
    // Use `web_sys`'s global `window` function to get a handle on the global
    // window object.
    let window = web_sys::window().expect("no global `window` exists");
    let document = window.document().expect("should have a document on window");
    let body = document.body().expect("document should have a body");

    let mut jid: String = match window.location().hash() {
        Ok(v) => match js_sys::decode_uri_component(&v) {
            Ok(w) => {
                String::from(w.replace("#", ""))
            },
            Err(_e) => FALLBACK_JID.to_string()
        },
        Err(_e) => FALLBACK_JID.to_string(),
    };

    if jid.is_empty() {
        jid = FALLBACK_JID.to_string();
    }

    document.set_title(&title_text(&jid));

    // <div class="main">
    let main = document.create_element("div")?;
    main.set_attribute("class", "main");

    // <h3 class="text-center" id="heading"></h3>
    let heading = document.create_element("h3")?;
    heading.set_attribute("class", "text-center");
    heading.set_inner_html(&heading_text(&jid));
    main.append_child(&heading).expect("Could not create heading");

    // <p class="text-center"><a class="btn btn-primary" id="button"><a></p>
    let button_area = document.create_element("p")?;
    button_area.set_attribute("class", "text-center");
    let button = document.create_element("a")?;
    button.set_attribute("class", "btn btn-primary");
    button.set_attribute("href", &jid_to_url(&jid));
    button.set_inner_html(&button_text(&jid));
    button_area.append_child(&button);
    main.append_child(&button_area).expect("Could not create button");

    let qrcode_area = document.create_element("div")?;
    qrcode_area.set_attribute("class", "text-center");
    let qrcode_obj = QrCode::new(&jid_to_url(&jid)).unwrap();
    let qrcode_svg = qrcode_obj.render()
        .min_dimensions(200, 200)
        .dark_color(svg::Color("#000000"))
        .light_color(svg::Color("#ffffff00"))
        .build();
    qrcode_area.set_inner_html(&qrcode_svg);
    main.append_child(&qrcode_area);

    //<input type="url" class="form-control text-center" id="url_in" readonly="">
    let url_in = document.create_element("input")?;
    url_in.set_attribute("type", "url");
    url_in.set_attribute("class", "form-control text-center");
    url_in.set_attribute("readonly", "");
    url_in.set_attribute("value", &jid_to_url(&jid));
    main.append_child(&url_in).expect("Could not create input");

    // <p class="hint text-center" id="clients"></p>
    let clients = document.create_element("p")?;
    clients.set_attribute("class", "hint text-center");
    clients.set_inner_html("If this link does not work, you need to install and configure an XMPP client, and visit this page again afterwards.");
    main.append_child(&clients).expect("Could not create client help text");

    // <p class="lead" id="recommend"></p>
    let recommend = document.create_element("p")?;
    recommend.set_attribute("class", "lead");
    recommend.set_inner_html("We recommend one of:");
    main.append_child(&recommend).expect("Could not create recommend text");

    // <ul class="lead" id="client_list"></ul>
    let client_list = document.create_element("ul")?;
    client_list.set_attribute("class", "lead");

    let clients_android = vec![
        Client { name: "Pix-Art Messenger", url: "https://play.google.com/store/apps/details?id=de.pixart.messenger", description: "modern and featureful" },
        Client { name: "Conversations", url: "https://play.google.com/store/apps/details?id=eu.siacs.conversations", description: "modern and featureful" },
        Client { name: "Xabber", url: "https://play.google.com/store/apps/details?id=com.xabber.android", description: "focus on user experience and interoperability" },
        Client { name: "Yaxim", url: "https://play.google.com/store/apps/details?id=com.xabber.android", description: "lightweight classic UI" },
        Client { name: "Bruno", url: "https://play.google.com/store/apps/details?id=org.yaxim.bruno", description: "cute and cuddly (based on yaxim)" }
    ];

    let clients_linux = vec![
        Client { name: "Dino", url: "https://dino.im/", description: "modern, clean and GNOME integrated" },
        Client { name: "Kaidan", url: "https://kaidan.im/", description: "modern, convergent and cross-platform" },
        Client { name: "Gajim", url: "https://gajim.org", description: "full-featured" }
    ];

    let clients_ios = vec![
        Client { name: "Chat Secure", url: "https://itunes.apple.com/us/app/chatsecure/id464200063", description: "Encrypted Messenger for iOS" },
    ];

    let clients_vec: Vec<Client>;
    if window.navigator().user_agent().unwrap().contains("Android") {
        clients_vec = clients_android;
    } else if window.navigator().user_agent().unwrap().contains("iPhone") {
        clients_vec = clients_ios;
    } else {
        clients_vec = clients_linux;
    }

    for client in clients_vec {
        let item = document.create_element("li")?;
        let strong = document.create_element("strong")?;
        let link = document.create_element("a")?;
        link.set_attribute("href", client.url);
        link.set_inner_html(client.name);
        strong.append_child(&link);
        item.append_child(&strong);

        let description = document.create_text_node(&format!(" - {}", client.description));
        item.append_child(&description);

        client_list.append_child(&item).expect("Could not append client");
    }

    main.append_child(&client_list);

    // <p class="lead" id="checkfulllist"></p>
    let checkfulllist = document.create_element("p")?;
    checkfulllist.set_attribute("class", "lead");
    checkfulllist.set_inner_html("Check the <a href='http://xmpp.org/software/clients.html'>full list of XMPP clients</a> for other platforms.");
    main.append_child(&checkfulllist).expect("Could not create check full list text");

    // <p class="hint" id="xmppis"></p>
    let xmppis = document.create_element("p")?;
    xmppis.set_attribute("class", "hint");
    xmppis.set_inner_html("XMPP is a provider independent form of instant messaging. You can pick from many different clients and have a free choice of server operators to participate.");
    main.append_child(&xmppis).expect("Could not create xmpp info");

    body.append_child(&main).expect("Could not create main div");

    Ok(())
}
