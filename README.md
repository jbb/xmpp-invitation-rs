## How to build
```
cargo install wasm-pack
wasm-pack build --target web
```

## Usage

```
python3 -m http.server
```
Then point a browser to http://localhost:8000/#name@server.tld
